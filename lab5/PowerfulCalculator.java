import java.util.*;

public class PowerfulCalculator {
	// operators set
	private static Character[] operatorChar = {'+', '-', '*'};
	private static ArrayList<Character> operatorList = new ArrayList<Character>(Arrays.asList(operatorChar));
	// numbers set
	private static Character[] numberChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	private static ArrayList<Character> numberList = new ArrayList<Character>(Arrays.asList(numberChar));
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			// get a formula from users
			System.out.println("Please input your formula");
			String formulaInput = sc.nextLine();
			
			if(formulaInput.equalsIgnoreCase("quit")) {
				sc.close();
				System.exit(0);
			}
			// validate the formula by calling the method named validate
			if(validate(formulaInput)) {
				// process the formula by calling the method named process 
				// and save the calculated results in an ArrayList
				//ArrayList<Integer> finalList = process(formulaInput);
			}
			else {
				System.out.println("Invalid formula.");
			}
		}		
	}
	
	private static boolean validate(String formula) {
		// check if formula has length of 0
		if(formula.length()<1) {
			return false;
		}
		// check if formula has operators at the beginning or end
		if(operatorList.contains(formula.charAt(0)) || operatorList.contains(formula.charAt(formula.length()-1))){
			return false;
		}
		// check if the formula contains invalid character
		for(int i = 0; i < formula.length(); i++) {
			char temp = formula.charAt(i);
			if(!numberList.contains(temp) && !operatorList.contains(temp)) {
				return false;
			}
			// check if the formula has neighboring operators
			else if(operatorList.contains(temp) && operatorList.contains(formula.charAt(i+1))) {
					return false;
				}
			}
		return true;
	}
	// recursive method to process the formula
	private static ArrayList<Integer> process(String formula) {
		ArrayList<Integer> finalList = new ArrayList<Integer>();
		boolean noOperators = true;
		//checks to see if there are operators left
		for(int i = 0; i<formula.length(); i++) {
			char temp = formula.charAt(i);
			if(operatorList.contains(temp)) {
				noOperators = false;
			}
		}
		//returns the final list if there are no operators left
		if(noOperators) {
			
		}
		
		return finalList; 
	}

	// more helper methods go here

}
