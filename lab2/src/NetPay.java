import java.util.InputMismatchException;
import java.util.Scanner;
import java.lang.IllegalArgumentException;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NetPay {
	
	public static final double FEDERAL_TAX_PERCENT = 10.00;
	public static final double STATE_TAX_PERCENT = 4.5;
	public static final double SS_PERCENT = 6.2;
	public static final double MEDICARE_PERCENT = 1.45;
	public static final double PAY_PER_HOUR = 7.25;

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		double hoursWorked = requestHours(userInput); //runs the scanner method to get number hours worked.
		createOutput(moneyCalc(hoursWorked), lineWriter() ); //uses the moneyCalc and lineWriter method to build
															 //the final output.	
	}
	
	public static double requestHours(Scanner userInput) {
		
			System.out.print("Hours per week: \t");
			double hoursWorked = 0;
			try { //test to see if hoursWorked is given a number
			hoursWorked = userInput.nextDouble();
			}catch(InputMismatchException e){ //throws and exception if the user enters a non numeric value
		    throw new IllegalArgumentException("Please enter a positive number, try again");
		    // tell them that the value must be positive
			}
			if(hoursWorked < 0) { //throws an exception if user enters negative number
				throw new IllegalArgumentException("The number must be zero or greater");
			}

		return hoursWorked; //returns the user's values
		
		
	}
	public static double[] moneyCalc(double hoursWorked) {
		double grossPay =(hoursWorked*PAY_PER_HOUR);
		double federalDeduction = grossPay *(FEDERAL_TAX_PERCENT /100);
		double stateDeduction = grossPay * (STATE_TAX_PERCENT /100);
		double ssDeduction = grossPay * (SS_PERCENT /100);
		double medicareDeduction = grossPay * (MEDICARE_PERCENT /100);
		double netPay = grossPay - federalDeduction - stateDeduction - ssDeduction - medicareDeduction;
		double[] payValues = { //creates an array of all the money values
				grossPay,
				netPay,
				federalDeduction,
				stateDeduction,
				ssDeduction,
				medicareDeduction
		};
		return payValues;
	}
	public static String[] lineWriter() {
		String[] outputStrings = { //creates an array of all of the statements, the written output
				"Gross Pay: \t \t",
				"Net Pay: \t \t",
				"Federal: \t \t",
				"State: \t \t \t",
				"Social Security: \t",
				"Medicare \t \t"
		};
		return outputStrings;
	}
	public static void createOutput(double[] payValues, String[] outputStrings) {
		DecimalFormat df = new DecimalFormat("#.######"); //test
		df.setRoundingMode(RoundingMode.DOWN);//test
		for(int i =0; i<payValues.length;i++) { //for loop repeats for the length of the arrays
			if(i==0) {
				System.out.println(outputStrings[0] + df.format(payValues[0])); //if statements check which line needs to be read
			}
			else if(i==1) {
				System.out.println(outputStrings[1] + df.format(payValues[1]));
				System.out.println(); //extra before deduction section
				System.out.println("Deductions"); //heading for the deduction section
			}
			else if(i==2) {
				System.out.println(outputStrings[2] + df.format(payValues[2]));
			}
			else if(i==3) {
				System.out.println(outputStrings[3] + df.format(payValues[3]));
			}
			else if(i==4) {
				System.out.println(outputStrings[4] + df.format(payValues[4]));
			}
			else if(i==5) {
				System.out.println(outputStrings[5] + df.format(payValues[5]));
			}
		}
	}
}