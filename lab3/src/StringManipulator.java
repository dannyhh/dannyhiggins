import java.util.Scanner;


public class StringManipulator {
	public static final String quit = "quit";
	public static final String reverse = "print reverse";
	public static final String remove = "remove";
	public static final String reAll = "replace all";
	public static final String reOne = "replace single";

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		enterCommand(welcomeMessage(sc));//runs the command check with the scanner
											 // and the user input from welcomeMessage

	}
	public static String welcomeMessage(Scanner sc) {
		System.out.println("Enter the string to be manipulated \t");
		String userWord = sc.nextLine();
		return userWord;
	}
	public static void enterCommand(String userWord) {
		Scanner sc = new Scanner(System.in); //builds a new scanner on each call to prevent error (not sure if necessary but fixed problem I was having)
		System.out.println("Enter your command "
				+ "(quit, print reverse, replace all, remove, replace single");
		String userCommand = sc.nextLine();
		
		if(userCommand.equalsIgnoreCase(quit)) {
			System.exit(0);
		}
		else if(userCommand.equalsIgnoreCase(reAll)) {
			replaceAll(userWord, sc);	
		}
			
		else if(userCommand.equalsIgnoreCase(remove)) {
			removeChar(userWord, sc);
		}
			
		else if(userCommand.equalsIgnoreCase(reOne)){
			replaceOne(userWord, sc);
		}

		else if(userCommand.equalsIgnoreCase(reverse)) {
			reverseString(userWord, sc);
		}
		else {
			System.out.println("Please enter a valid command");
			enterCommand(userWord);
		}
	}

	public static void replaceAll(String userWord, Scanner sc) {
		char[] tempOutput = userWord.toCharArray();
		System.out.println("Enter the charecter to replace");
		String replaceThis= sc.nextLine();//takes in char to be replaced as string
		
		if(!userWord.contains(replaceThis)) { //checks that the char exists
			System.out.println("Error: the letter you are trying to replace does not exist");	
			enterCommand(userWord);	
		}
		else if(replaceThis.length() > 1) {
			System.out.println("Please enter only one charecter");
			enterCommand(userWord);
		}
		System.out.println("Enter the new charecter");
		String replaceWithString = sc.nextLine();//takes in char to replace with as lower case

		 if(replaceWithString.length() >1) {
			System.out.println("Please enter only one charecter");
			enterCommand(userWord);
		}
		char replaceWith = replaceWithString.charAt(0); //turns the char to replace with into a string
		for(int i = userWord.length()-1; i>=0; i--) {
			String charString = ""; //creates empty string
			charString += userWord.charAt(i); //turns the char enter into a String
			
			if(charString.equals(replaceThis)) { //tests if chars are the same and lower case
				tempOutput[i] = replaceWith;
			}
		}
		String tempOutputString = String.valueOf(tempOutput);
		userWord = tempOutputString; //sets the user word to the new word 
		System.out.println("The new String is:" + userWord);
		enterCommand(userWord);	
	}
	
	public static void removeChar(String userWord, Scanner sc) {
		System.out.println("Enter the charecter to remove");
		String removeThis = sc.nextLine();
		if(removeThis.length() > 1) {
			System.out.println("Please enter only one charecter");
			enterCommand(userWord);
		}
		else if(!userWord.contains(removeThis)) {
			System.out.println("Error: the letter you are trying to remove does not exist");	
			enterCommand(userWord);
		}

		userWord = userWord.replace(removeThis, "");
		System.out.println("The new String is:" + userWord);
		enterCommand(userWord);	
	}
	
	public static void reverseString(String userWord, Scanner sc) {
		String tempOutput = "";
		for(int i = userWord.length()-1; i>=0; i--) { //for loop repeats from last index
			tempOutput += userWord.charAt(i);		  //down to zero
		}
		System.out.println("The new String is:" + tempOutput);
		enterCommand(userWord);	
	}
	
	public static void replaceOne(String userWord, Scanner sc) {
		char[] tempOutput = userWord.toCharArray(); //creates a char array of the input string
		
		System.out.println("Enter the charecter to replace");
		String replaceThisString= sc.nextLine();//takes in char to be replaced as string
		char replaceThis = replaceThisString.charAt(0);
		if(!userWord.contains(replaceThisString)) { //checks that the char exists
			System.out.println("Error: the letter you are trying to replace does not exist");	
			enterCommand(userWord);	
		}
		else if(replaceThisString.length() > 1) {
			System.out.println("Please enter only one charecter");
			enterCommand(userWord);
		}
		
		System.out.println("Enter the new charecter");
		String replaceWithString = sc.nextLine();
		char replaceWith = replaceWithString.charAt(0); //turns the char to replace with into a string
		if(replaceWithString.length() >1) {
			System.out.println("Please enter only one charecter");
			enterCommand(userWord);
		}
		
		System.out.println("Which " + replaceThis +" would you like to replace?" );
		int replaceNumber = sc.nextInt();
		if(replaceNumber > userWord.length()-1 || replaceNumber <= 0) { //checks for errors with numbers
			System.out.println("Error: The letter you are trying to replace does not exist");
			enterCommand(userWord);
		}
		replaceNumber--; //decreases replaceNumber by 1 to get it to an index value;
		

		int lengthCounter = 0;
		for(int i = 0; i<=userWord.length()-1; i++) { //builds an array of index values 
			if(userWord.charAt(i) == replaceThis) {
				lengthCounter++;
			}
		}
		
		int[] charIndex = new int[lengthCounter];
		int charCounter =0;
		for(int i = 0; i<userWord.length(); i++) { //builds an array of index values 
			if(userWord.charAt(i) == replaceThis) {
				charIndex[charCounter] = i;
				charCounter++;
			}
		}
		if(lengthCounter <= replaceNumber) {
			System.out.println("Error: the letter you are trying to replace does not exist");
			sc.nextLine();
			enterCommand(userWord);
		}
		
		tempOutput[charIndex[replaceNumber]] = replaceWith;
		String tempOutputString = String.valueOf(tempOutput);
		userWord = tempOutputString; //sets the user word to the new word 
		System.out.println("The new String is:" + userWord);
		enterCommand(userWord);	
		}
}	
