//*******************************************************
// Circle.java
//Daniel Higgins 10/31/17
// The purpose of this program is to manipulate a given circle.
//This program is written by Daniel Higgins and is not intended for any others to use.
//*******************************************************
public class Circle {

	
	private double radius; //creates empty variable for radius
	private double x; //creates an empty variable for x, which represents center of circle
	private double y; //creates an empty variable for y, which represents center of circle

	//----------------------------------------------
	// getX - returns the value of x
	//----------------------------------------------
	public double getX() {
		return this.x;
		//returns the x value
	}
	

	//----------------------------------------------
	// getY - returns the value of y
	//----------------------------------------------
	public double getY() {
		return this.y;
		//returns the y value	
	}
	
	//----------------------------------------------
	// getRadius - returns the value of radius
	//----------------------------------------------
	public double getRadius() {
		return this.radius;
		// returns the radius	
	}

	//----------------------------------------------
	// setX - assigns a new value to x
	//----------------------------------------------
	public void setX(double newXValue) {
		this.x = newXValue;
		// changes the x value to a new value
	}
	

	//----------------------------------------------
	// setY - assigns a new value to y
	//----------------------------------------------
	public void setY(double newYValue) {
		this.y = newYValue;
		//changes the y value to a new value	
	}	
	
	
	//----------------------------------------------
	// setRadius - assigns a new value to radius
	//----------------------------------------------
	public void setRadius(double newRadiusValue) {
		if(newRadiusValue >=0) {
			this.radius = newRadiusValue;
		}
		//If the radius value is valid (greater or equal to zero)
		//sets the value to the new value, else it stays the same
	}
	
	//--------------------------------------------------------
	// diameter - calculates the diameter of the circle
	//--------------------------------------------------------
	public double diameter() {
		double diameter = 2*radius;
		//calclates the diameter
		return diameter;
		//returns the diameter	
	}
	

	//--------------------------------------------------------
	// area - returns the area of the circle
	//--------------------------------------------------------
	public double area() {
		double area = Math.PI * Math.pow(radius, 2);
		//calculates the area
		return area;
		//return the area	
	}

	//--------------------------------------------------------
	// perimeter - returns the perimeter of the circle
	//--------------------------------------------------------
	public double perimeter() {
		double perimeter = 2*Math.PI*radius;
		//calculates the perimeter
		return perimeter;
		//returns the perimeter
	}
	
	//--------------------------------------------------------
	// isUnitCircle - return true if the radius of this circle
	//                is 1 and its center is (0,0) and false
	//      	      otherwise.
	//--------------------------------------------------------
	public boolean isUnitCircle() {
		if(radius ==1 && x ==0 && y==0) {
			//checks if the radius is 1 and the origin is 0,0
			return true;
			//returns true
		}
		else {
			//if its not a unit circle
			return false;
			//return false
		}
	}
	
	
	//--------------------------------------------------------
	// toString - return a String representation of
	//            this circle in the following format:
	//            center:(x,y)
	//            radius: r
	//--------------------------------------------------------
	public String toString() {
		String center = "center: (" + this.x + "," + this.y + ")";
		//creates a string telling the user of the center point
		String radius = "radius: " + this.radius;
		//creates a string telling the user the radius
		
		return "\n" + center + "\n"+ radius;
		//returns a string that lists the center point leaves a blank line then 
		//displays the radius
		
	}
	public boolean equals(Circle anotherCircle) {
		if(toString().equals(anotherCircle.toString())) {
			//checks if the strings for each are the same 
			return true;
		}
		else {
			//if there not the same returns false
			return false;
		}
	}
	public boolean isConcentric(Circle anotherCircle) {
		if(getX() == anotherCircle.getX() && getY() == anotherCircle.getY()) {
			//if the center is the same
			return true;
		}
		else {
			//otherwise return false
			return false;
		}
	}
	public double distance(Circle anotherCircle) {
		double distanceX = getX() - anotherCircle.getX();
		//finds the distance between X points
		double distanceY = getY() - anotherCircle.getY();
		//finds the distance between Y points
		return Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));
		//returns the value of the square root of both x and y distances squared and added 
	}
	public boolean intersects(Circle anotherCircle) {
		if(distance(anotherCircle) < getRadius()+anotherCircle.getRadius()) {
			return true;
		}
		else {
			return false;
		}
	}
		
	//}

}
