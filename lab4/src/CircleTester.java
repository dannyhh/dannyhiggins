//*******************************************************
// CircleTester.java
//
//
//  A client to test the functionality of objects
//  of the class Circle
// 
//*******************************************************
public class CircleTester{
	
	public static void main(String[] args) {
		
		Circle circle1 = new Circle();
		Circle circle2 = new Circle();
		circle1.setX(0.0);
		circle1.setY(0.0);
		circle1.setRadius(2);
		circle2.setX(2.0);
		circle2.setY(1.0);
		circle2.setRadius(1);
		System.out.println("circle1="+circle1);
		System.out.println("circle2="+circle2);
		
		// If the method setRadius is implemented correctly,
		// a call to setRadius with a negative number
		// will not change the value of the circle's radius.
		//
		circle1.setRadius(-2.0); 
		
		//
		// Reset the center of circle1 (-3.0,4.0)
		//
		circle1.setX(-3.0);
		circle1.setY(4.0);
		
		System.out.println("circle1="+circle1);
		//java recognizes that it needs to convert circle1 to a string, so
		//using .toString() is not needed
		
		circle2.setRadius(5.3);
		// set the circle2 radius to 5.3
		
		System.out.println("circle2="+circle2);
		// print circle2 characteristics (center and radius)
		
		System.out.println("diameter: " + circle1.diameter() +" \n area: " + circle1.area() +
				"\n perimeter: " + circle1.perimeter());
		// print circle1 diameter, area and perimeter
		
		System.out.println("diameter: " + circle2.diameter() +" \n area: " + circle2.area() +
				"\n perimeter: " + circle2.perimeter());
		// print circle2 diameter, area and perimeter
		
		if(circle1.isUnitCircle() ==true) {
			System.out.println("Circle 1 is a unit circle");
		}
		else {
			System.out.println("Circle 1 is not a unit circle");
		}
		// display whether circle1 is a unit circle
		
		if(circle2.isUnitCircle() ==true) {
			System.out.println("Circle 2 is a unit circle");
		}
		else {
			System.out.println("Circle 2 is not a unit circle");
		}
		// display whether circle2 is a unit circle
		
		// your additional tests should be placed below here
		System.out.println("\n \n \n");
		//linebreak so its easier for me to check results
		
		//Test 1 for equals
		double temp =0;
		if(circle1.equals(circle2)==true && circle1.getX() == circle2.getX() && circle1.getY() == circle2.getY()) {
			System.out.println("All values of the circle are the same");
		}
		else if(circle1.equals(circle2)) {
			System.out.println("The circles only have the same radius");
		}

		else {
			temp = circle1.getRadius()-circle2.getRadius();
			System.out.println("The difference in radii is: " + Math.abs(temp) );
		}
		//Test 2 for isConcentric
		boolean whileCounter = false;
		while(whileCounter == false) {
			if(circle1.isConcentric(circle2) && circle1.getRadius() == circle2.getRadius()) {
				System.out.println("The circle is concentric and identical");
				whileCounter=true;
			}
			else if(circle1.isConcentric(circle2)){
				System.out.println("The circle is just concentric");
				whileCounter =true;
			}	
			else if(!circle1.isConcentric(circle2)){
				System.out.println("Changing values");
				circle1.setX(10);
				circle1.setY(10);
				circle2.setX(10);
				circle2.setY(10);
			}
		}
		//Test 3 for distance
		if(circle1.distance(circle2) == 0) {
			System.out.println("The circles are in the same location");
		}
		if(circle1.distance(circle2)%10 ==0 ) {
			System.out.println("The distance is divisible by 10");
		}
		if(circle1.distance(circle2) != 0) {
			System.out.println("The circles are not in the same location, setting both to unit circles");
			circle1.setX(0);
			circle1.setY(0);
			circle1.setRadius(1);
			circle2.setX(0);
			circle2.setY(0);
			circle2.setRadius(1);
		}
		//Test 3 for intersects
		while(circle1.intersects(circle2) == true) {
			System.out.println("The circles intersect, changing values");
			circle1.setX(circle1.getX()*10+1);
			circle1.setY(circle1.getY()*10+1);
		}
		Circle circle3 = new Circle();
		circle3.setRadius(5);
		circle3.setX(5);
		circle3.setY(5);
		if(circle3.intersects(circle1) && circle3.intersects(circle2)) {
			System.out.print("Circle 3 intersects both");
		}
		else if(circle3.intersects(circle1)) {
			System.out.println("Cirlce 3 intersects circle 1");
		}
		else if(circle3.intersects(circle2)) {
			System.out.println("Cirlce 3 intersects circle 2");
		}
		else {
			System.out.println("Circle 3 does not intersect circles 1 or 2");
		}


	}
}
	

