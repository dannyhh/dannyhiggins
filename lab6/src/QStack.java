import java.util.LinkedList;

public class QStack {
	private LinkedList<Integer> stack;
	
	public QStack(LinkedList<Integer> inputStack) {
		stack = inputStack;
	}
	public void push(int i) {//pushes the int into the queue
		stack.add(i);	
	}
	public int pop() { //gets and removes the last from the queue as defined on javadoc
		int temp = stack.getLast();
		stack.removeLast();
		return temp;
	}
	public int top() {//gets the last as defined on Queue javadoc
		return stack.getLast();
	}
	public boolean isEmpty() {//checks if it is empty
		if(stack.size()==0) {
			return true;
		}
		return false;
	}
}
