import java.util.LinkedList;

public class QStackTest {

	public static void main(String[] args) {
		//creates the 3 linkedlists for testing
		LinkedList <Integer> q1 = new LinkedList <Integer>();
		LinkedList <Integer> q2 = new LinkedList <Integer>();
		LinkedList <Integer> q3 = new LinkedList <Integer>();
		//creates the 3 QStacks for testing 
		QStack test1 = new QStack(q1);
		QStack test2 = new QStack(q2);
		QStack test3 = new QStack(q3);
		//calls the 3 tests
		case1(test1);
		case2(test2);
		case3(test3);
	}
	public static void case1(QStack test) {
		for(int i = 1; i<6; i++) {
			test.push(i);
		}
		for(int i = 0; i<3; i++) {
			test.pop();
		}
		System.out.println(test.top());
		
		
	}
	public static void case2(QStack test) {
		test.push(2);
		test.push(4);
		test.push(8);
		for(int i = 0; i<2; i++) {
			test.pop();
		}	
		System.out.println(test.isEmpty());
	}
	public static void case3(QStack test) {
		for(int i = 1; i<4; i++) {
			test.push(i*3);
		}
		test.pop();
		System.out.println(test.top());
		System.out.println(test.isEmpty());
	}

}
