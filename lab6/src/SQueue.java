import java.util.Stack;

public class SQueue {
	private Stack<Integer> queue;

	public SQueue(Stack<Integer> inputQueue) {
		queue = inputQueue;
	}
	public void push(int x) {
		//creates a queue like stack, ie first value pushed is top value
		Stack<Integer> tempStack = new Stack<Integer>();
		while(!queue.isEmpty()) { //while loop to remove previous vals
			tempStack.push(queue.pop());
		}
		queue.push(x); //adds new value to the bottom
		while(!tempStack.isEmpty()) {//while loop to push previous values
			queue.push(tempStack.pop());
		}
	}
	public int pop() {//pops the top value
		return queue.pop();
	}
	public int peek() {//peeks the top value
		return queue.peek();		
	}
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	
}
