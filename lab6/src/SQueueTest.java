import java.util.Stack;

public class SQueueTest {

	public static void main(String[] args) {
		//creates the 3 stacks for testing
		Stack<Integer> s1 = new Stack<Integer>();
		Stack<Integer> s2 = new Stack<Integer>();
		Stack<Integer> s3 = new Stack<Integer>();
		//creates the 3 SQueues for testing
		SQueue test1 = new SQueue(s1);
		SQueue test2 = new SQueue(s2);
		SQueue test3 = new SQueue(s3);
		//calls the 3 tests
		case1(test1);
		case2(test2);
		case3(test3);
	}
	//test 1
	public static void case1(SQueue test) {
		for(int i =1; i <6; i++) {
			test.push(i);
		}
		for(int i =0; i <3; i++) {
			test.pop();
		}
		System.out.println(test.peek());
	}
	//test 2
	public static void case2(SQueue test) {
		test.push(2);
		test.push(4);
		test.push(8);
		for(int i =0; i <2; i++) {
			test.pop();
		}
		System.out.println(test.isEmpty());
	}
	//test 3
	public static void case3(SQueue test) {
		test.push(3);
		test.push(6);
		test.push(9);
		test.pop();
		System.out.println(test.peek());
		System.out.println(test.isEmpty());
	}

}
